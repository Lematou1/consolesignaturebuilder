﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleArchiver
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Wait for data path, string");
                var path = Console.ReadLine();
               // var path = @"d:/film.avi";
                var fileSize = new FileInfo(path).Length;
                Console.WriteLine("Wait for block size, int && > 0 && <= " + fileSize / (uint)Environment.ProcessorCount);
                var blockSize = Console.ReadLine();
                //var blockSize = 50000;
                if (Convert.ToInt32(blockSize) <= 0 ||
                    Convert.ToInt32(blockSize) > fileSize) throw new Exception("block size must be greater than 0");
                var mmf = MemoryMappedFile.CreateFromFile(path);
                
                   var accessor = mmf.CreateViewAccessor();

                    var builder = new SignatureBuilder()
                    {
                        BlockSize = Convert.ToInt32(blockSize),
                        FileSize = (int)fileSize,
                        MemoryMappedViewAccessor = accessor
                    };
                    GetSignaturesChunked(FormObjectList(builder));
                  //accessor.Dispose();
               // mmf.Dispose();
                
                
                      
            Console.ReadLine();
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.ReadLine();
            }
        }

        public static void GetSignatureThreading(object builder)
        {            
                var builderT = (SignatureBuilder)builder;         
            
                SignatureBuilder.GetSignature(builderT.MemoryMappedViewAccessor, builderT.BlockSize, builderT.FileSize, builderT.StartPosition, builderT.EndPosition);           
        }

        public static void GetSignaturesChunked(List<SignatureBuilder> list)
        {
            var i = 0;
            foreach(var builder in list)
            {
                Thread myThread = new Thread(new ParameterizedThreadStart(GetSignatureThreading));
                myThread.Name = i.ToString();
                myThread.Start(builder);
                i++;
            }
        }

        public static List<SignatureBuilder> FormObjectList(object builder)
        {
            var builderT = (SignatureBuilder)builder;
            var proccessorCount = (uint)Environment.ProcessorCount;
            var tmp = builderT.FileSize / proccessorCount;
            builderT.StartPosition = 0;
            builderT.EndPosition = (int)tmp;
            var list = new List<SignatureBuilder>();
            for (int i = 0; i < proccessorCount; i++)
            {
                list.Add(new SignatureBuilder() {
                    MemoryMappedViewAccessor = builderT.MemoryMappedViewAccessor,
                    BlockSize = builderT.BlockSize,
                    FileSize = builderT.FileSize,
                    StartPosition = builderT.StartPosition,
                    EndPosition = builderT.EndPosition
                });
                builderT.StartPosition = builderT.EndPosition;
                builderT.EndPosition += (int)tmp;
            }
            return list;
        }
       
    }
}
