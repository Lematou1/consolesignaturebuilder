﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleArchiver
{
    internal class Block
    {
        internal int Number { get; set; }
        internal byte[] Array { get; set; }
    }
}
