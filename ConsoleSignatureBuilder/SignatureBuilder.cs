﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleArchiver
{
    class SignatureBuilder
    {
        private static object threadLock = new object();


        public MemoryMappedViewAccessor MemoryMappedViewAccessor;
        public int BlockSize;
        public int FileSize;
        public int StartPosition;
        public int EndPosition;
       // private static byte[] ByteArray;

        public SignatureBuilder()
        {

        }

        public SignatureBuilder(SignatureBuilder builder)
        {
            MemoryMappedViewAccessor = builder.MemoryMappedViewAccessor;
            BlockSize = builder.BlockSize;
            FileSize = builder.FileSize;
            StartPosition = builder.StartPosition;
            EndPosition = builder.EndPosition;
           // ByteArray = new byte[builder.FileSize];
        }

        internal static void GetSignature(MemoryMappedViewAccessor accessor, int blockSize, int fileSize, int startPosition, int endPosition)
        {


            //var tmpp = binReader.ReadBytes(endPosition - startPosition);
            //stream.ReadArray(startPosition, new MemoryStream(), 0, endPosition - startPosition);
            var byteArray = new byte[endPosition - startPosition];
            accessor.ReadArray(startPosition, byteArray, 0, endPosition - startPosition);
            
           
                var tmp = SplitArrayByBlocks(byteArray, blockSize, startPosition, endPosition);
                ConsoleOut(tmp);
                
            
        }

        internal static List<Block> SplitArrayByBlocks(byte[] byteArray, int blockSize, int startPosition, int endPosition)
        {
            var resultList = new List<Block>();

                for (var i = 0; i < (byteArray.Length / blockSize); i++)
                {

                   // var t = byteArray(endPosition - startPosition);
                    var tempArray = new byte[blockSize];
                    //var str = new StreamReader();
                    Array.Copy(byteArray, i * blockSize, tempArray, 0, blockSize);
                    resultList.Add(new Block() { Number = Convert.ToInt32(Thread.CurrentThread.Name) * (byteArray.Length / blockSize) + i, Array = tempArray });
                }
            
            return resultList;
        }

        internal static void ConsoleOut(List<Block> listBlock)
        {
            var mySHA256 = SHA256.Create();
            

            foreach (var block in listBlock)
            {
                lock (block)
                {
                    var myHashBytes = mySHA256.ComputeHash(block.Array);
                    StringBuilder stringBuilder = new StringBuilder();

                    foreach (byte b in myHashBytes)
                        stringBuilder.AppendFormat("{0:X2}", b);

                    string hashString = stringBuilder.ToString();
                    //Thread.CurrentThread.GetHashCode().ToString() + " = " + 
                    Console.WriteLine(block.Number + " :: " + hashString);
                }
            }
            mySHA256.Dispose();
        }
    }
}
